package com.example.android.pickmeup;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText username, password;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = (EditText)findViewById(R.id.editTextuser);
        password = (EditText)findViewById(R.id.editTextpassword);
        btnLogin = (Button)findViewById(R.id.button);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    String usernameKey = username.getText().toString();
                    String passwordKey = password.getText().toString();

                    if(usernameKey.equals("admin") && passwordKey.equals("admin") || usernameKey.equals("kel2") && passwordKey.equals("02")){
                        Toast.makeText(getApplicationContext(),"Login Sukses...!",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, MenuUtamaActivity.class);
                        String nama = String.valueOf(usernameKey);
                        intent.putExtra("username", nama);
                        MainActivity.this.startActivity(intent);
                    }else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("Username atau Password Anda Salah...!")
                                .setNegativeButton("Retry...",null).create().show();
                    }
            }
        });

    }

    public void signUp(View view) {
        Intent intent = new Intent(MainActivity.this,MainActivity2.class);
        MainActivity.this.startActivity(intent);
    }
}

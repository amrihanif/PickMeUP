package com.example.android.pickmeup;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class MenuUtamaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);
    }

    public void pick(View view) {
        {
            Intent ab = new Intent(MenuUtamaActivity.this, MenuPickActivity.class); // untuk membuka activity daftar menu
            startActivity(ab);
        }
    }

    public void post(View view) {
        {
            Intent ab = new Intent(MenuUtamaActivity.this, MenuPostActivity.class); // untuk membuka activity daftar menu
            startActivity(ab);
        }
    }
}